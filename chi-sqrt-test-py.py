#!/usr/bin/env python
# coding: utf-8

# https://pythonfordatascience.org/chi-square-test-of-independence-python/
# #### p-value - second value

# In[27]:


import pandas as pd
import numpy as np
from scipy.stats import chisquare
from scipy import stats


# In[31]:


df = pd.read_csv('chi_r.csv')

df
# In[32]:


crosstab = pd.crosstab(df.Ynat,df.Yl)


# In[33]:


stats.chi2_contingency(crosstab)


# In[ ]:




